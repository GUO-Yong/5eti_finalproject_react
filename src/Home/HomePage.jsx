import React from "react";
import ParticlesBg from "particles-bg";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";

import { styled } from "@mui/material/styles";
import Button from "@mui/material/Button";
import { purple, teal,lightBlue } from "@mui/material/colors";
import { useNavigate } from "react-router-dom";

import { useDispatch } from "react-redux";
import{UrlAction} from '../reducer/Action';

export const HomePage = (props) => {
  const navigate = useNavigate();
   const dispatch = useDispatch();



  const TeamButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(purple[500]),
    backgroundColor: purple[500],
    "&:hover": {
      backgroundColor: purple[700],
    },
  }));

  const EquipButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(teal[500]),
    backgroundColor: teal[500],
    "&:hover": {
      backgroundColor: teal[700],
    },
  }));
  const WikiButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(lightBlue[500]),
    backgroundColor: lightBlue[500],
    "&:hover": {
      backgroundColor: lightBlue[700],
    },
  }));

  function _handleButton(e) {
    let button = e.currentTarget.innerText.toString();

    switch (button.toLowerCase()) {
      case "select team":
        dispatch(UrlAction("/team"))
        navigate("/team");
        break;
      case "select equipment":
        dispatch(UrlAction("/equipment"))
        navigate("/equipment");
        break;
      case "wiki":
        navigate("/cardwiki");
        break;
      default:
        break;
    }
  }

  return (
    
    <div>
      
<ParticlesBg type="circle" bg={{
    position:"fixed",
    left:0,
    top:0,
    zIndex: -1,
    width:"100%",
    height:"100%",
}}    />


      <Box sx={{ width: "100%" }}>

        <Typography
          variant="h1"
          component="div"
          gutterBottom
          color="white"
          fontSize="12rem"
        >
          PICTURE  ARENA
        </Typography>

        <Grid
          container
          spacing={0}
          width="100%"
          direction="row"
          justifyContent="center"
          alignItems="stretch"
        >
          <Grid item xs={4}>
            <TeamButton variant="contained" onClick={(e) => _handleButton(e)}>
              Select Team
            </TeamButton>
          </Grid>
          <Grid item xs={4}>
            <EquipButton variant="contained" onClick={(e) => _handleButton(e)}>
              Select Equipment
            </EquipButton>

          </Grid>

          <Grid item xs={4}>
            <WikiButton variant="contained" onClick={(e) => _handleButton(e)}>
              Wiki
            </WikiButton>
            
          </Grid>
        </Grid>
      </Box>



    

 </div>
  );
};
