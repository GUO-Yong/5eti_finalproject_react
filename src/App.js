
import React from 'react';
import { Provider  } from 'react-redux';
import store from './reducer/store';

import {MainRouter} from './MainRouter'
function App() {

  return (
  <Provider store={store}>
    <div><MainRouter/></div>
</Provider>
  );
}



export default App;
