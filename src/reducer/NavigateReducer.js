export const NavigateReducer =(state={url: ""},action) =>{
    switch(action.type){

        case "updateUrl":
            return {url:action.url};

        default:
            return state;
    }


}