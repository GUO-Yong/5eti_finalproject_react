//enregistrer les cartes sélectionnées 
export const CardReducer =(state={selectedCards: []},action) =>{
    let cardList ;
    switch(action.type){
        case "post":
            if(state.selectedCards.length<3){
            cardList = state.selectedCards.concat(action.cardInfos);
            //console.log("cardList_state :----------------------");
            //console.log(cardList);
            //state.selectedCards.push(action.cardInfos)
            return {selectedCards: cardList};}
            else{return state;}

        case "delete":
            cardList = state.selectedCards;
            //garder les cartes qui n'ont Id different que celle qu'on veur supprimer
            cardList= cardList.filter(card=>card.cardId !== action.cardInfos.cardId);
            return {selectedCards:cardList};

        case "clean":
            return {selectedCards:[]};
        default:
            return state;
    }


}