
export const Action =(type,x)=>{
    return {type:type,cardInfos:x};
}
export const LoginAction = (islogin,userInfos)=>{
    return {type:"update",isLogin:islogin,user:userInfos};
}

export const EquipAction =(type,x)=>{
    return {type:type,equipInfos:x};
}

export const UrlAction =(x)=>{
    return {type:"updateUrl",url:x};
}