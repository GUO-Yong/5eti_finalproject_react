import {createStore} from 'redux';
import { combineReducers } from 'redux';
import {CardReducer} from './CardReducer';
import {UserReducer} from './UserReducer';
import { EquipReducer } from './EquipReducer';
import {NavigateReducer} from "./NavigateReducer";
const globalReducer = combineReducers({
  cardReducer: CardReducer,
  userReducer : UserReducer,
  equipReducer:EquipReducer,
  navigateReducer:NavigateReducer,
});


export const store = createStore(globalReducer);
export default store;
