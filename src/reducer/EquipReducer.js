//enregistrer les cartes sélectionnées 
export const EquipReducer =(state={selectedEquips: []},action) =>{
    let cardList ;
    switch(action.type){
        case "postEquip":
            if(state.selectedEquips.length<3){

            cardList = state.selectedEquips.concat(action.equipInfos);
            return {selectedEquips: cardList};}

            //console.log("cardList_state :----------------------");
            //console.log(cardList);
            //state.selectedCards.push(action.cardInfos)
            else{ return state;}

        case "deleteEquip":
            cardList = state.selectedEquips;
            //garder les cartes qui n'ont Id different que celle qu'on veur supprimer
            cardList= cardList.filter(card=>card.cardId !== action.equipInfos.cardId)
            return {selectedEquips:cardList};

        case "clean":
                return {selectedEquips:[]};
        default:
            return state;
    }


}