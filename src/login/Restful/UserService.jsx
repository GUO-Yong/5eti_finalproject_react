//port de nginx
//const baseUrl = "http://localhost:3333";

const baseUrl =" http://54.152.148.26:80";

var token = "";


export function checkUserLogin(userLogin) {
  const urlLogin = baseUrl + "/login";

  let myInit = {
    method: "POST",

    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(userLogin),
  };

  return fetch(urlLogin, myInit)
    .then((response) => {
      if (response.status !== 200) {

        return false;
      }
      return response.json();
    })
    .then((data) => {
      if(data){
      token = data.token;
      return data;
    }
    else{
      return false;
    }
    })
    .catch((error) => {
      console.log(error);
    });
}

export function getUserInfos(login, tokenValue) {
  let urlUserInfos = baseUrl + "/users/" + login;

  let header = new Headers({
    "Authorization": "Bearer " + token,
    "Access-Control-Allow-Credentials": "true",
    "Access-Control-Allow-Origin": "*",
  });

  let myInit = {
    method: "GET",
    mode: "cors", //--------------------------------------------------
    credentials: "include",
    headers: header,
  };
  return fetch(urlUserInfos, myInit)
    .then((response) => {
      if (response.status !== 200) {
        throw new Error(
          `There was an error in getUserInfos with status code ${response.status}`
        );
      }
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((error) => {
      console.log(error);
    });
}

export function registerUser(userInfos) {
  let urlRegisterUser = baseUrl + "/users/register";

  let myInit = {
    method: "POST",
    credentials: "include",

    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Credentials": "true",
      "Access-Control-Allow-Origin": "*",
    },
    body: JSON.stringify(userInfos),
  };

  return fetch(urlRegisterUser, myInit).then((response) => {   if (response.status !== 200) {
    throw new Error(
      `There was an error  in registerUser with status code ${response.status}`
    );
  }
    return response.text();
  });
}

// list of team : [{ "cardId": 33,"cardType": "ANIMAL"}]
export function updateUserTeam(username,teamList,isEquip) {
  let urlUpdateTeam =
    baseUrl + "/users/"  + username + "/team";
  
  if(isEquip){
    urlUpdateTeam = urlUpdateTeam + "/equip";
  }

  
  let myInit = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Authorization": "Bearer " + token,
      "Access-Control-Allow-Credentials": "true",
      "Access-Control-Allow-Origin": "*",
    },
    body: JSON.stringify(teamList),
  };
  console.log("url Equip ",urlUpdateTeam);
  console.log("list Equip ",teamList);

  

  return fetch(urlUpdateTeam , myInit).then((response) => {
    if (response.status !== 200) {
      throw new Error(
        `There was an error  in updateUserTeam isEquip ${isEquip} with status code ${response.status}`
      );
    }
    return response.text();
  })
}



export function getUserTeam(username){
  let urlTeam = baseUrl + "/users/"  + username + "/team";
  let myInit = {
    method: "GET",
    headers: {
      "Authorization": "Bearer " + token,
      "Content-Type": "application/json",
      "Access-Control-Allow-Credentials": "true",
      "Access-Control-Allow-Origin": "*",
    },
  };

  return fetch(urlTeam, myInit).then((response) => {
    if (response.status !== 200) {
      throw new Error(
        `There was an error  in updateUserTeam with status code ${response.status}`
      );
    }
    return response.json();
  })

}