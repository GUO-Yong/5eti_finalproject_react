import Popup from "reactjs-popup";
import "reactjs-popup/dist/index.css";


export const PopupBox=(props)=>{
    return (
        <Popup
            trigger={open => (
              <button className="secondary"  >Register</button>
            )}
            position="center center"
            closeOnDocumentClick
            disabled={props.showPopup}
          
          >
            <span>{props.message}</span>
          </Popup>
          
    );
}