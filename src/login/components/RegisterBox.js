import React ,{useState}  from 'react';
import logo from "../../images/logo.png";

import "./Box.css";
import {registerUser} from '../Restful/UserService';
import { useNavigate } from "react-router-dom";
import { PopupBox } from './Popup';

export const RegisterBox =(props)=>{
  const navigate = useNavigate();

  const [showPopup, setShowPopup] = useState(false);
  const [messageTip, setMessage] = useState("");


    function handleSubmit (e) {
        e.preventDefault();
        const login = e.target.login.value;
        const password=e.target.password.value;
        const confirmPassword = e.target.confirmPassword.value;
     
     

        if (!login) {
          alert("login is required");
        } else if (!password) {
          alert("Password is required");
        }  else if(password !== confirmPassword) {
          alert("Check your password");
        }
        else{
          registerUser({username:login,password:password})
              .then(response=>{
                console.log("response register",response);
                if(response==="true"){
                  console.log("user created");
                  //TODO update user

                  setShowPopup(true);
                  setMessage("Done");
        navigate("/login");
                }
                else{
                  
                  console.log("error: user not created");
                  setShowPopup(true);
                  setMessage("Error");
                
                }

               })

        }

      };
    

    return (
        <div className="LoginBox">
        <img src={logo} className="logo" alt="logo" />
        <form className="form" onSubmit={handleSubmit}>

          <div className="input-group">
            <label htmlFor="text">Login</label>
            <input type="text" name="login"   />
          </div>


          <div className="input-group">
            <label htmlFor="password">password</label>
            <input type="password" name="password" />
            <input style={{marginTop:"2px"}} type="password" name="confirmPassword"  placeholder="Confirm your password"/>
          </div>
          <button className="secondary"  >Register</button>
          
       
        </form>

        {!showPopup? <></>:<div>{messageTip}</div> }
      
      </div>
    );
};