import React,{useState} from "react";
import logo from "../../images/logo.png";
import "./Box.css";
import { Loading } from "../../loading/Loading";

import { useNavigate } from "react-router-dom";
import { checkUserLogin, getUserInfos } from "../Restful/UserService";
import "reactjs-popup/dist/index.css";
import { useDispatch, useSelector } from "react-redux";
import { LoginAction } from "../../reducer/Action";

function wait(ms){
  var start = new Date().getTime();
  var end = start;
  while(end < start + ms) {
    end = new Date().getTime();
 }
}

export const LoginBox = (props) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const state_url = useSelector(
    (state) => state.navigateReducer.url
  );
  const [isloading,setLoading]=useState(false);


  function _handleSubmit(e) {
    e.preventDefault();
    setLoading(true);
    const login = e.target.login.value;
    const password = e.target.password.value;

    let userInfos = {
      login: login, //Local version login   / "admin"
      password: password, //Local version password /"pwdadmin"
    };

    if (!e.target.login.value) {
      alert("login is required");
    } else if (!e.target.password.value) {
      alert("Password is required");
    } else {
      checkUserLogin(userInfos).then((data) => {

        /*
        //supprimer  TESTs Local version
        const testUser = { userId: 1, username: "admin" };
        dispatch(LoginAction(true, testUser));
        
        navigate(state_url);
        */
       //---------------------
        
          if (data.token) {
            getUserInfos(userInfos.login, data.token)
              .then(data => {
                dispatch(LoginAction(true, data));
                        navigate(state_url);


              })
          }
          else{
            console.log("fail to login");
          setLoading(true);
          wait(1000);
          navigate("/login");

          }

          
          //------------------------
      });
    }
  }

  function _handleRegisterClick(e) {
    e.preventDefault();
    navigate("/register");
  }
  if(isloading){
    return <Loading/>;
  }
else{

  return (
  
    <div className="LoginBox">
      <img src={logo} className="logo" alt="logo" />
      <form className="form" onSubmit={_handleSubmit}>
        <div className="input-group">
          <label htmlFor="text"> Login </label>{" "}
          <input type="text" name="login" />
        </div>
        <div className="input-group">
          <label htmlFor="password"> password </label>{" "}
          <input type="password" name="password" />
        </div>
        <button className="primary"> LOGIN </button>{" "}
      </form>{" "}
      <button className="secondary" onClick={_handleRegisterClick}>
        Register
      </button>
    </div>
  );

}
};
