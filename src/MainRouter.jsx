
import { BrowserRouter as Router, Route , Routes, Navigate } from "react-router-dom";
import {LoginBox} from './login/components/LoginBox';
import {RegisterBox} from './login/components/RegisterBox';
import { CardSelectScreen } from "./card/CardSelectScreen";
import { CardEquipScreen } from "./card/CardEquipScreen";
import {CardWiki} from "./card/CardWiki";
import {useSelector} from 'react-redux';
import { NavBar } from "./navBar/NavBar";


import {HomePage} from "./Home/HomePage";
export const  MainRouter=(props)=> {
    let connected = useSelector(state => state.userReducer.isLogin);

    /*
  
    */
      return (
        <Router>
            <Routes>
                <Route exact path="login" element={<LoginBox/>} />
                <Route exact path="register" element={<RegisterBox/>} />

                <Route exact path="home" element={<HomePage/>} />
                <Route exact path="cardwiki" element={ <div><NavBar/><CardWiki/></div>} />



                { connected && 
                    <Route path="team" element={
                    <div><NavBar/><CardSelectScreen/></div>
                    }
                    />
                }  
    

                
                { connected && 
                    <Route path="equipment" element={
                    <div><NavBar/><CardEquipScreen/></div>
                    }
                    />
                }

{ !connected && 
                    <Route path="team" element={
                    <div><LoginBox/></div>
                    }
                    />
                }  
    

                
                { !connected && 
                    <Route path="equipment" element={
                    <div><LoginBox/></div>
                    }
                    />
                }


                { !connected && 
                    <Route path="*" element={<Navigate to="/home" />}/>
                }      
            </Routes>
        </Router>
      );
    }
