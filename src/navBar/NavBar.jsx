import logo from "../images/smallLogo.png";
import React from "react";
import { useSelector ,useDispatch} from "react-redux";

import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import MenuItem from "@mui/material/MenuItem";
import{UrlAction,Action} from '../reducer/Action';

import { useNavigate } from "react-router-dom";
import {LoginAction}from "../reducer/Action";


function stringToColor(string) {
  let hash = 0;
  let i;

  /* eslint-disable no-bitwise */
  for (i = 0; i < string.length; i += 1) {
    hash = string.charCodeAt(i) + ((hash << 5) - hash);
  }

  let color = "#";

  for (i = 0; i < 3; i += 1) {
    const value = (hash >> (i * 8)) & 0xff;
    color += `00${value.toString(16)}`.substr(-2);
  }
  /* eslint-enable no-bitwise */

  return color;
}

function stringAvatar(name) {
  if (name.split(" ").length > 1) {
    return {
      sx: {
        bgcolor: stringToColor(name),
      },
      children: `${name.split(" ")[0][0]}${name.split(" ")[1][0]}`,
    };
  } else {
    return {
      sx: {
        bgcolor: stringToColor(name),
      },
      children: `${name.split(" ")[0][0]}`,
    };
  }
}

const pages = ["Select Team", "Select Equipement","Wiki"];
const settings = ["Profile", "Account", "Dashboard", "Logout"];

export const NavBar = () => {
   const dispatch = useDispatch();
  const navigate = useNavigate();

  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);




  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const _handleShowMenuButton = (page) => {
    switch (page ) {
      case "Select Team":
        dispatch(UrlAction("/team"))
      navigate("/team");
        break;
      
     case "Select Equipement":
      dispatch(UrlAction("/equipment"))

      navigate("/equipment");
      break;
    case "Wiki":
    navigate("/cardwiki");
    break;
    default:
      break;
  }
};
  
  const handleCloseNavMenu =()=>{
    setAnchorElNav(null);
  }
  const _handleHideMenuButton =(page)=>{
    _handleShowMenuButton(page);
  }

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };
  const handleMenuItem = (e) => {
    setAnchorElUser(null);
    console.log(e.target.innerHTML);
    let logout = e.target.innerHTML;
    if(logout ==="Logout"){
      dispatch(LoginAction(false,{}));

    dispatch( Action("clean", null));
    }
  };



  var state_user = useSelector((state) => state.userReducer.user);

  return (
    <AppBar style={{  position:"fixed",
      top:0,
     }} sx={{ bgcolor: "white" }}>
      <Container>
        <Toolbar disableGutters>
          <img alt="logo" src={logo} height="50px" onClick={()=>{navigate("/home");}} style={{ cursor:"pointer"}}  />

          <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
              
            >
              <MenuIcon/>
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "left",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: "block", md: "none", },
              }}
            >
              {pages.map((page) => (
                <MenuItem key={page} onClick={()=> _handleHideMenuButton(page)}>
                  <Typography textAlign="center">{page}</Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>
          <Typography
            variant="h6"
            noWrap
            component="div"
            sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}
          >
            LOGO
          </Typography>
          <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
            {pages.map((page) => (
              <Button
                key={page}
                name="button"
                onClick={()=>_handleShowMenuButton(page)}
                sx={{
                  display: "inline",
                  fontWeight: "bold",
                  mx: 0.5,
                  fontSize: 20,
                }}
              >
                {page}
              </Button>
            ))}
          </Box>

          <Box style={{ marginRight: "10px" }}>
            <Typography textAlign="center" color="black">
              { state_user && state_user.username ?state_user.username:null}
            </Typography>
          </Box>

          <Box sx={{ flexGrow: 0 }}>
            <Tooltip title="Open settings">
              <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
              {state_user && state_user.username ? <Avatar {...stringAvatar(`${state_user.username}`)} />:<></>}


               
              </IconButton>
            </Tooltip>

            <Menu
              sx={{ mt: "45px" }}
              id="menu-appbar"
              anchorEl={anchorElUser}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}
            >
              {settings.map((setting) => (
                <MenuItem key={setting} onClick={handleMenuItem}>
                  <Typography textAlign="center">{setting}</Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};
