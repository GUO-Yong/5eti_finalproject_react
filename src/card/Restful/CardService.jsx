import Card from "../components/CardClass";

const urlRef = "http://127.0.0.1:8083/initCardRef";
const urlModel = "http://127.0.0.1:8083/initCardModel";

//const urlBase = "http://127.0.0.1:8080";
const urlBase = "http://204.236.239.109:80";
export function initCards() {
  var myHeaders = new Headers();

  var myInit = {
    method: "GET",
    headers: myHeaders,
    mode: "cors",
    cache: "default",
  };

  fetch(urlRef, myInit).then((response) => {
    fetch(urlModel, myInit).then((response) => {
      console.log("2fecth");
    });
    return response.body;
  });
}

export function getUserCards(userId,type) {
  //type = play / equip
  let urlgetUserCards = urlBase+"/cardsFight/"+type;
  urlgetUserCards = urlgetUserCards + "/" + userId;

  var myInit = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  };

  console.log("fetch getUserCards");
  return fetch(urlgetUserCards, myInit)
    .then((response) => {
      if (response.status !== 200) {
        throw new Error(
          `There was an error in getUserCards with status code ${response.status}`
        );
      }
      return response.json();
    })
    .then((data) => {
  console.log("fetch getUserCards",data);

      return data;
    })
    .catch((error) => {
      console.log(error);
    });
}

export function convertModel(array_cards) {
  let converted_cards = [];
  array_cards.map((card) => {
    //card classe change!
    let cardClass = new Card(
      card.owner,
      card.id,
      card.type,
      card.urlImgWL,
      card.attack,
      card.hp,
      card.speedattack,
      card.value
    )
    converted_cards.push(cardClass.cardInfos);
    return card;
  });
  return converted_cards;
}

export function getCardById(cardId) {
  let urlCardById = urlBase+"/card/" + cardId;

  var myInit = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  };
  return fetch(urlCardById, myInit)
    .then((response) => {
      if (response.status !== 200) {
        throw new Error(
          `There was an error in getUserCards with status code ${response.status}`
        );
      }
      return response.json();
    })
    .then((card) => {
      //change cardClass
      let cardClass = new Card(
        card.owner,
        card.id,
        card.type,
        card.urlImgWL,
        card.attack,
        card.hp,
        card.speedattack,
        card.value
      );
      return cardClass;
    })
    .catch((error) => {
      console.log(error);
    });
}

export function getCardRef(label) {

  let url = urlBase+"/cardref";
  if (label) {
    url = url + "/" + label;
  }

  var myInit = {
    method: "GET",
    headers: { "Content-Type": "applicatioin/json" },
  };
  return fetch(url, myInit)
    .then((response) => {
      if (response.status !== 200) {
        throw new Error(
          `There was an error in getCardRef with status code ${response.status}`
        );
      }
      return response.json();
    })
    .catch((error) => console.log(error));
}
