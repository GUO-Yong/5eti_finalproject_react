import  React from 'react';

import {Typography} from '@mui/material';
import {makeStyles} from '@mui/styles'
import Grid from '@mui/material/Grid';
import {Icon} from '@iconify/react';

const userStyles = makeStyles((theme) => ({
    typo: {
        paddingRight: "10px"
    }
}));
export const CardInfos = (props) => {

    const classes = userStyles();

    return (
        <div>
            <Grid
                container
                direction="row"
                justifyContent="center"
                alignItems="center"
                spacing={1}>
                <Icon icon="ph:sword-fill" color="black" height="30"/>
                <Typography variant="h5" className={classes.typo}>
                    {props.information.attack}
                </Typography>

                <Icon icon="ci:heart-fill" color="red" height="30"/>
                <Typography variant="h5" className={classes.typo}>
                {props.information.hp}

                </Typography>

                <Icon icon="cil:stream" color="black" height="30"/>
                <Typography variant="h5" className={classes.typo}>
                {props.information.speed}
                </Typography>

                <Icon icon="akar-icons:coin" color="#fde20c" width="30" />
                <Typography variant="h5" className={classes.typo}>
                {props.information.value}
                </Typography>

            </Grid>
        </div>
    )
};
