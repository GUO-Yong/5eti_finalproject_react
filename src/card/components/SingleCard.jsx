import React from "react";
import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import { CardActionArea, CardActions } from "@mui/material";
import { CardInfos } from "./CardInfos";
import { Action, EquipAction } from "../../reducer/Action";

import { useDispatch } from "react-redux";
/*
const blueStyles = {
  border: 4,
  borderColor: "primary.main",
};

const whiteStyles = {
  border: 4,
  borderColor: "white",
};
*/


export const SingleCard = (props) => {
  const dispatch = useDispatch();




  function _addCard2Store(cardInfos,isEquip) {
    let action;
    if(isEquip){
      action = EquipAction("postEquip", cardInfos)
    }
    else{action = Action("post", cardInfos);}
    dispatch(action);


  }

  function _deleteCard2Store(cardInfos,isEquip) {
    let action;
    if(isEquip){
      action = EquipAction("deleteEquip", cardInfos)
    }
    else{action = Action("delete", cardInfos);}
    dispatch(action);
  }


 

function _handleClick(){
  if(props.isBlock === true){return}
  if(!props.borderSet){
    _deleteCard2Store(props.cardInfos,props.isEquip);

  }

  else{

    if(props.borderStyle.borderColor==="white"){
      _addCard2Store(props.cardInfos,props.isEquip);
      //setStyle(blueStyles);

    }
    else{
    _deleteCard2Store(props.cardInfos,props.isEquip);
    //setStyle(whiteStyles);
 
    }

  }
}


  return (
    <Card
      sx={{
        ...props.borderStyle,
        maxWidth: 345,
        borderRadius: "20px",
      }}
      style={{
        padding: "2px",
        margin: "3px",
      }}
      onClick={_handleClick}
    >
      <CardActionArea border="3px">
        <CardMedia
          component="img"
          //image={require('../images/dog1.png')}
          //image = {props.cardInfos.imageUrl}
          src={props.cardInfos.imageUrl}
          alt="card"
          height="300px"
        />
      </CardActionArea>
      <CardActions
        sx={{
          justifyContent: "center",
        }}
      >
        <CardInfos information={props.cardInfos} />
      </CardActions>
    </Card>
  );
};
