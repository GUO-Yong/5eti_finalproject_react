import React from "react";
import { SingleCard } from "./SingleCard";

import Grid from "@mui/material/Grid";


const blueStyles = {
  border: 4,
  borderColor: "primary.main",
};

const whiteStyles = {
  border: 4,
  borderColor: "white",
};

export const DisplayAllCards = (props) => {
  /*
  props.borderSet;

  props.cardList;
   props.stateList;
  */
  

  var borderStyle={
      border: 4,
      borderColor: "white",
    };


    
if(!props.borderSet){
  borderStyle.borderColor="white";
  return (   <Grid
    container
    direction="row"
    justifyContent="center"
    alignItems="center"
    spacing={1}
  >
    {props.cardList.map((card) => {

      return (
        <SingleCard
          
          key={card.cardId}
          cardInfos={card}
          borderSet={false}
          borderStyle={borderStyle}
          isEquip={props.isEquip}
        />
      );
    })}
  </Grid>
  );
}


else{

  return (
    <div>
    
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        spacing={1}
      >
        {props.cardList.map((card) => {


     let ans = props.stateList.filter(stateCard=>stateCard.cardId === card.cardId);


     if(ans.length >0 &&props.borderSet ){
      borderStyle=blueStyles;
     }
      
       else{   borderStyle =whiteStyles;}
 
          return (
            <SingleCard
              key={card.cardId}
              cardInfos={card}
              borderSet={props.borderSet}
          isEquip={props.isEquip}
          borderStyle={borderStyle}

            />
          );
        })}
      </Grid>
    </div>
  );

}
};
