export class Card {

    constructor(userId,cardId,type, imageUrl, attack, hp, speed,value) {
        this.userId = userId;
        this.cardId = cardId;
        this.type=type;
        this.imageUrl = imageUrl;
        this.hp = hp;
        this.speed = speed;
        this.attack = attack;
        this.value=value;

    }
    get cardIds(){
        return{
            userId:this.userId,
            cardId:this.cardId
        }
    }
    get cardInfos() {
        return {
            userId:this.userId,
            cardId:this.cardId,
            type:this.type,
            imageUrl: this.imageUrl,
            hp: this.hp,
            speed: this.speed,
            attack: this.attack,
            value:this.value,
        }
    }


};
export default Card;
