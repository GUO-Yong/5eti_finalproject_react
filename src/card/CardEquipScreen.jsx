import React, { useState, useEffect , useReducer } from "react";

import { DisplayAllCards } from "./components/DisplayAllCards";
import { SingleCard } from "./components/SingleCard";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import { useSelector } from "react-redux";
import { getUserCards, convertModel, getCardById } from "./Restful/CardService";
import { updateUserTeam, getUserTeam } from "../login/Restful/UserService";
import Box from "@mui/material/Box";

import Typography from "@mui/material/Typography";

//TODO userId

export const CardEquipScreen = (props) => {
  const [, forceUpdate] = useReducer(x => x + 1, 0);
  const state_selectedEquip = useSelector(
    (state) => state.equipReducer.selectedEquips
  );

  var state_user = useSelector((state) => state.userReducer.user);

  //initCards();
  const [userCards, setUserCards] = useState([]);
  const [isLoading, setLoading] = useState(true);

  const [userTeam, setUserTeam] = useState([]);

  const [isTeamLoading, setTeamLoading] = useState(true);

  useEffect(() => {
    getInfosPage()
  }, []);

  function getInfosPage(){

        //get userTeam
        getUserTeam(state_user.username).then((data)=>{
          var cardList =[];
          data.map((id)=> {   getCardById(id).then((card)=>{cardList.push(card)})});
          console.log("cardList Team",cardList);
          setUserTeam(cardList);
          
          getUserCards(state_user.userId,"equip")
          .then((data) => convertModel(data))
          .then((data) => {
            console.log("data equip",data);
            setUserCards(data);

            setTeamLoading(false);
            setLoading(false);
      
          });

          handleForce();
    
    
        })
  }

  function handleForce() {
    forceUpdate();
  }


  function _handleSubmit() {
    console.log(state_selectedEquip);
    var equipList=[];

    state_selectedEquip.map((card) => {
      equipList.push({cardId:card.cardId,cardType:card.type})
    });

    while(equipList.length<3){
      equipList.push({cardId:0,cardType:"EQUIPMENT"})
    }

    console.log("Submit equipList",equipList)
    updateUserTeam(state_user.username,equipList,true).then(
      (response) => {
        console.log("Update Equip Done response", response, );
      }
    );




  }



  if (isLoading | isTeamLoading) {
    return <div>Loading...</div>;
  }

  return (
    //change userCards by team
    <div style={{ marginTop: "1500px" }}>
      <Grid container direction="row" justifyContent="center" spacing={1}>
        <Box>
          <Typography
            textAlign="center"
            sx={{ color: "text.primary", fontSize: 34, fontWeight: "medium" }}
          >
            Current Team
          </Typography>

 <Grid
    container
    direction="row"
    justifyContent="center"
    alignItems="center"
    spacing={1}
  >
            { userTeam.length > 0? (userTeam.map((card) => (
                <SingleCard
                  key={card.cardInfos.cardId}
                  cardInfos={card.cardInfos}
                  borderSet={false}
                  isBlock={true}
                />
            ))
              ):<div>{userTeam}</div>  }

              </Grid>
          <Typography
            textAlign="center"
            sx={{ color: "text.primary", fontSize: 34, fontWeight: "medium" }}
          >
            Equipments
          </Typography>

          <Grid
    container
    direction="row"
    justifyContent="center"
    alignItems="center"
    spacing={1}
  >
      
            {state_selectedEquip.length > 0 ? (
               <DisplayAllCards cardList={state_selectedEquip} borderSet={false} stateList={state_selectedEquip} isEquip={true} />

            ) : (
              <div>{state_selectedEquip}</div>
            )}
        </Grid>
        </Box>
      </Grid>

      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          margin: "5px",
        }}
      >
        <Button variant="contained" onClick={_handleSubmit}>
          Submit
        </Button>
      </div>

      <hr style={{ margin: "50px", width: "100%" }} />

      <Grid container justifyContent="center" spacing={1}>
        <Typography
          textAlign="center"
          sx={{ color: "text.primary", fontSize: 34, fontWeight: "medium" }}
        >
          All Equipments
        </Typography>

        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="center"
          spacing={1}
        >
      <DisplayAllCards cardList={userCards} borderSet={true} stateList={state_selectedEquip} isEquip={true} />

        </Grid>
      </Grid>
    </div>
  );
};
