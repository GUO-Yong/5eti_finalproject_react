import { getCardRef } from "./Restful/CardService";
import React, { useState, useEffect } from "react";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import { styled } from "@mui/material/styles";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import ButtonBase from "@mui/material/ButtonBase";
import { deepPurple } from "@mui/material/colors";
import Button from "@mui/material/Button";

import { Icon } from "@iconify/react";


import dog from "../images/animal/dog2.png";

const Img = styled("img")({
  margin: "auto",
  display: "block",
  maxWidth: "100%",
  maxHeight: "100%",
});

const RandomButton = styled(Button)(({ theme }) => ({
  color: theme.palette.getContrastText(deepPurple[800]),
  backgroundColor: deepPurple[800],
  "&:hover": {
    backgroundColor: deepPurple[900],
  },
}));



export const CardWiki = (props) => {
  const [isLoading, setLoading] = useState(true);
  const [template_array, setTemplate] = useState([]);
  const [selectedCard, setSelectedCard] = useState({});
  const [isFirst, setFirst] = useState(false);


  useEffect(() => {
    getCardRef().then((data) => {
      console.log("data",data);

      if(typeof data === 'undefined' ){
        return null;
      }
      setTemplate(data);
      setLoading(false);

    });
  }, []);
  

  const handleTag = (e: React.SyntheticEvent, value: []) => {
    
    let filter =template_array.filter((i) => i.name === value.name);
      if(filter.length>0){

        setSelectedCard(filter[0]);
        setFirst(true);
      }

  };

  function getRandomInt(max) {
    return Math.floor(Math.random() * max);
  }

  function handleRandom(){
    
    let len = template_array.length-1;
    let index = getRandomInt(len);
    console.log("random",template_array[index]);
    setSelectedCard(template_array[index]);
    setFirst(true);

  }

  function handleNext() {
    let index = template_array.findIndex((i) => i.name === selectedCard.name);
    let next;
    next = index + 1 < template_array.length ? index + 1 : 0;
    setSelectedCard(template_array[next]);
  }

  if (isLoading) {

    return <div>...</div>;
  } else {
    return (
      <Grid
        container
        spacing={2}
        direction="column"
        justifyContent="center"
        alignItems="center"
      >
        <Grid item>
          <Autocomplete
            style={{ position: "relative", height: "200px", top: 10 }}
            disablePortal
            id="combo-box-demo"
            options={template_array}
            sx={{ width: 500, height: 500 }}
            getOptionLabel={(option) => option.name}
            isOptionEqualToValue={(option) => option.name}
            onChange={handleTag}
            renderInput={(params) => <TextField {...params}  label="Search" />}
          />
        </Grid>
        <RandomButton variant="contained" onClick={handleRandom}>
Random 
</RandomButton>



        <Grid item>
          {isFirst? (
            <Paper sx={{ p: 2, flexGrow: 1, width: "800px" }}>
              <Grid container spacing={2}>
                <Grid item>
                  <ButtonBase>
                    <Img alt="complex" height="500px" width="500px" src={dog} />
                  </ButtonBase>
                </Grid>
                <Grid item xs={12} sm container>
                  <Grid item xs container direction="column" spacing={2}>
                    <Grid container direction="column" spacing={1} xs>
                      <Grid
                        container
                        xs
                        justifyContent="center"
                        alignItems="center"
                      >
                        <Typography
                          gutterBottom
                          variant="subtitle1"
                          component="div"
                        >
                          {selectedCard.name}
                        </Typography>
                      </Grid>

                      <Grid
                        container
                        xs
                        justifyContent="center"
                        alignItems="center"
                      >
                        <Typography variant="h5" gutterBottom>
                          {selectedCard.rarete}
                        </Typography>
                      </Grid>
                      <Grid
                        xs
                        container
                        direction="row"
                        justifyContent="center"
                        alignItems="center"
                      >
                        <Icon icon="ph:sword-fill" color="black" height="70" />

                        <Typography variant="h4" color="text.secondary">
                          {selectedCard.attack}
                        </Typography>
                      </Grid>
                      <Grid
                        xs
                        container
                        direction="row "
                        justifyContent="center"
                        alignItems="center"
                      >
                        <Icon icon="ci:heart-fill" color="red" height="70" />

                        <Typography variant="h4" color="text.secondary">
                          {selectedCard.hp}
                        </Typography>
                      </Grid>

                      <Grid
                        xs
                        container
                        direction="row"
                        justifyContent="center"
                        alignItems="center"
                      >
                        <Icon icon="cil:stream" color="black" height="70" />

                        <Typography
                          variant="h4"
                          color="text.secondary"
                        >
                          {selectedCard.speedattack}
                        </Typography>
                      </Grid>
                    </Grid>

                    <Grid
                      container
                      direction="row"
                      justifyContent="flex-end"
                      alignItems="center"
                    >
                      <Typography
                        sx={{ cursor: "pointer" }}
                        variant="h4"
                        onClick={handleNext}
                      >
                        Next
                      </Typography>
                    </Grid>
                  </Grid>
                  <Grid item>
                    <Icon icon="akar-icons:coin" color="#fde20c" width="30" />

                    <Typography variant="subtitle1" component="div">
                      ${selectedCard.value}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          ) : (
            <></>
          )}
        </Grid>
      </Grid>
    );
  }
};
