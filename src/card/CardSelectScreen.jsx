import React, { useState, useEffect } from "react";

import { DisplayAllCards } from "./components/DisplayAllCards";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import { useSelector } from "react-redux";
import { getUserCards, convertModel } from "./Restful/CardService";
import { updateUserTeam } from "../login/Restful/UserService";
//TODO userId

export const CardSelectScreen = (props) => {
  const state_selectedCards = useSelector(
    (state) => state.cardReducer.selectedCards
  );

  var state_user = useSelector((state) => state.userReducer.user);

  //initCards();
  const [userCards, setUserCards] = useState([]);
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    getUserCards(state_user.userId,"play")
      .then((data) => convertModel(data))
      .then((data) => {
        setUserCards(data);
        setLoading(false);
      });
  }, []);


  function _handleSubmit() {
    let teamList =[]
    //TODO handleSubmit
  
    state_selectedCards.map((card) => {
      teamList.push({cardId:card.cardId,cardType:card.type})
    });
  
    updateUserTeam(state_user.username,teamList,false).then(
        (response) => {
          console.log("updateUserTeam response", response, );
        }
      );
  }

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <div style={{ marginTop: "1000px" }}>
      <Grid container direction="row" justifyContent="center" spacing={1}>
        {state_selectedCards.length > 0 ? (
         <DisplayAllCards cardList={state_selectedCards} borderSet={false} stateList={state_selectedCards}  isEquip={false}/>
        ) : (
          <div>
            <p style={{ marginBottom: "150px" }}> Select your team</p>
          </div>
        )}
      </Grid>

      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          margin: "5px",
        }}
      >
        <Button variant="contained" onClick={_handleSubmit}>
          Submit
        </Button>
      </div>

      <hr style={{ margin: "50px", width: "100%" }} />


      <DisplayAllCards cardList={userCards} borderSet={true} stateList={state_selectedCards} isEquip={false} />

    </div>
  );
};
